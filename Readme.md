# Developement bootstrap

## Goal

This ansible playbook will install and configure a biased list of tools commonly used by power users using GNU/Linux and Open Source Softwares on a daily basis.

These tools are command-line tools, they can be used on any unix-based system (Linux, MacOs). They are the best option if you want to have a consistent developpment envirronment over time and different linux flavors (this playbook is not compatible with MacOsX).

Installed packages :

- vim to edit whatever you want
- git to handle your source code
- tmux, a terminal multiplexer
- htop to monitor your system
- powerline fonts
- tldr to have simple command line help
- zsh for a powerfull terminal shell

Tools configured by this playbook in your home directory (every config files already present will be backed up by ansible) :

- vim
- tmux
- git

## Howto

### Check that you can use sudo

    vm@machine $ sudo whoami
    password for vm:
    root

If not, refer to the documentation of your distribution to install and configure sudo.

### If you already have git installed on your system

    $ git clone https://gitlab.com/e-henry/dev-bootstrap.git

### If you don't have git or don't know (yet) what it is

1. Download this repository via the 'cloud' button on the top right of this page
2. Save it in your home directory
3. Unzip it

### Install ansible

On a Debian based system :  

    sudo apt update
    sudo apt install ansible

On an Archlinux based system :  

    sudo pacman -S ansible

On Fedora :  

    sudo dnf install ansible


### Run the script

Once ansible is installes, open a terminal and go to the directory where this project is and launch ansible

    $ ansible-playbook dev-bootstrap.yml

Ansible will ask for your password in order to install the packages.

## Todo

- Install pyenv
- Install nvm
- Install and configure zsh and oh-my-zsh
- Install pretty font for the terminal (and icons)
- Install some usefull scripts in ~/bin
